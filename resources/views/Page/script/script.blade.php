<script type="text/javascript">
    new Swiper('.testimonials-slider', {
        speed: 600
        , loop: true
        , slidesPerView: 'auto'
        , autoplay: {
            delay: 3000
            , disableOnInteraction: false
        }
        , breakpoints: {
            320: {
                slidesPerView: 1
                , spaceBetween: 20
            },

            1200: {
                slidesPerView: 1
                , spaceBetween: 20
            }
        }
        , centeredSlides: true
        , scrollbar: {
            el: '.swiper-scrollbar'
            , draggable: true
        , }
        , freeMode: {
            enabled: true
            , sticky: true
        , }
        , parallax: true
        , navigation: {
            nextEl: '.swiper-button-next'
            , prevEl: '.swiper-button-prev'
        , }
    });

    new Swiper('.portfolio-details-slider', {
        speed: 600
        , loop: true
        , slidesPerView: 'auto'
        , autoplay: {
            delay: 3000
            , disableOnInteraction: false
        }
        , breakpoints: {
            320: {
                slidesPerView: 1
                , spaceBetween: 20
            },

            1200: {
                slidesPerView: 1
                , spaceBetween: 20
            }
        }
        , centeredSlides: true
        , scrollbar: {
            el: '.swiper-scrollbar'
            , draggable: true
        , }
        , freeMode: {
            enabled: true
            , sticky: true
        , }
        , parallax: true
        , navigation: {
            nextEl: '.swiper-button-next'
            , prevEl: '.swiper-button-prev'
        , }
        , pagination: {
            el: '.swiper-pagination'
            , type: 'bullets'
            , clickable: true
        }
    });

    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    $(document).ready(function() {

        $("#floating_standard2").bind('keyup change', function() {

            check_Password($("#floating_standard").val(), $("#floating_standard2").val())


        })

        $("button").click(function() {

            check_Password($("#floating_standard").val(), $("#floating_standard2").val())

        });
    })

    function check_Password(Pass, Con_Pass) {

        if (Pass === "") {



        } else if (Pass === Con_Pass) {

            $(".cta-btn").prop('disabled', false);
            $('#alert-reset').hide()

        } else {
            $(".cta-btn").prop('disabled', true);
            $("#confirm_password").focus()
            $('#alert-reset').show()
            $("#alert-reset").html('<div class="alert alert-danger">{{ __("messages.kata_sandi_tidak_cocok") }}!</div>')

        }

    }

    $(".expand-toggle").click(function(e) {
        e.preventDefault();

        var $this = $(this);
        var expandHeight = $this.prev().find(".inner-bit").height();

        if ($this.prev().hasClass("expanded")) {
            $this.prev().removeClass("expanded");
            $this.prev().attr("style", "");
            $this.html("{{ __('messages.tampilkan_lebih_banyak') }}");
        } else {
            $this.prev().addClass("expanded");
            $this.prev().css("max-height", expandHeight);
            $this.html("{{ __('messages.tampilkan_lebih_sedikit') }}");
        }
    });

</script>

<script type="text/javascript">
    // REALTIME //
    function load_data_footer() {
        $.ajax({
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            , url: "{{ route('realtime.footer') }}"
            , type: "POST"
            , data: {
                "_token": $('meta[name="csrf-token"]').attr('content')
            }
            , dataType: "JSON"
            , success: function(resp_load_foot) {
                $('#footer_about').html(resp_load_foot.abouts);
                $(".expand-toggle").click(function(e) {
                    e.preventDefault();

                    var $this = $(this);
                    var expandHeight = $this.prev().find(".inner-bit").height();

                    if ($this.prev().hasClass("expanded")) {
                        $this.prev().removeClass("expanded");
                        $this.prev().attr("style", "");
                        $this.html("{{ __('messages.tampilkan_lebih_banyak') }}");
                    } else {
                        $this.prev().addClass("expanded");
                        $this.prev().css("max-height", expandHeight);
                        $this.html("{{ __('messages.tampilkan_lebih_sedikit') }}");
                    }
                });

                if (resp_load_foot.status_payment == true) {
                    $('#list-payment').removeClass('d-none');
                    $('#list-payment').html(resp_load_foot.result_payment);
                } else {
                    $('#list-payment').addClass('d-none');
                }
            }
        });
    }

    load_data_footer();

    $('.cart-btn').on('click', function() {
        $(".bayar-btn").addClass("d-none");
        $(".grt").addClass('d-none');
        $.ajax({
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            , url: "{{ route('realtime.cart') }}"
            , type: "POST"
            , cache: false
            , data: {
                "_token": $('meta[name="csrf-token"]').attr('content')
            }
            , dataType: "JSON"
            , beforeSend: function() {
                $("#cart-list").html('<div class="alert alert-info">{{ __("messages.please_wait") }}</div>');
            }
            , success: function(resp_load_cart) {
                $('#cart-list').html(resp_load_cart.result);
                $(".bayar-btn").removeClass("d-none");
                $(".grt").removeClass('d-none');
                $(".count_cart").html("(" + resp_load_cart.count_cart + ")");
            }
            , error: function() {
                $("#cart-list").html('<div class="alert alert-info"><center><img src="{{ config("app.url") . "/assets/img/avatar404.png" }}" class="rounded-full img-fluid" alt="{{ __("messages.keranjang_kosong") }}" title="{{ __("messages.keranjang_kosong") }}"><br />{{ __("messages.keranjang_kosong") }}</center></div>');
            }
        });
    });

    function load_data_banner() {
        $.ajax({
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            , url: "{{ route('realtime.banner') }}"
            , type: "POST"
            , data: {
                "_token": $('meta[name="csrf-token"]').attr('content')
            }
            , dataType: "JSON"
            , success: function(resp_load_banner) {
                $('#list-banner').html(resp_load_banner.result);
            }
        });
    }

    load_data_banner();

    $(".expand-toggle").click(function(e) {
        e.preventDefault();

        var $this = $(this);
        var expandHeight = $this.prev().find(".inner-bit").height();

        if ($this.prev().hasClass("expanded")) {
            $this.prev().removeClass("expanded");
            $this.prev().attr("style", "");
            $this.html("{{ __('messages.tampilkan_lebih_banyak') }}");
        } else {
            $this.prev().addClass("expanded");
            $this.prev().css("max-height", expandHeight);
            $this.html("{{ __('messages.tampilkan_lebih_sedikit') }}");
        }
    });

</script>

@if (auth()->user())
<script>
    function load_data_cart() {
        $.ajax({
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            , url: "{{ route('realtime.cart') }}"
            , type: "POST"
            , cache: false
            , data: {
                "_token": $('meta[name="csrf-token"]').attr('content')
            }
            , dataType: "JSON"
            , success: function(resp_load_cart) {
                $(".count_cart").html("(" + resp_load_cart.count_cart + ")");
            }
        });
    }

    load_data_cart();

    $(document).ready(function() {
        load_data_cart("yes");
    });

    setInterval(() => {
        load_data_cart();
    }, 3000);

    function load_data_grand_total() {
        $.ajax({
            header: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            , url: "{{ route('realtime.grandtotal') }}"
            , type: "POST"
            , cache: false
            , data: {
                "_token": $('meta[name="csrf-token"]').attr('content')
            }
            , dataType: "JSON"
            , success: function(resp_load_grand) {
                $(".grand-total").html(resp_load_grand.data1);
            }
        });
    }

    load_data_grand_total();

    // $(document).ready(function() {
    //     load_data_grand_total("yes");
    // });

    // setInterval(() => {
    //     load_data_grand_total();
    // }, 3000);

</script>
@endif
