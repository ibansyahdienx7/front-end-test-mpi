<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Traits\MyHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController extends Controller
{
    use MyHelper;

    function index()
    {
        return view('content');
    }

    function sign(LoginRequest $request)
    {
        try {
            $email = $request->email;
            $password = $request->password;

            $identity = User::where(
                'email',
                $email
            )->first();

            if (empty($identity)) {
                return Redirect::back()->with('error', __('messages.akun_tidak_ditemukan'));
            }

            if ($identity->status == 0) {
                return redirect(route('index'))->with('info', __('messages.pengguna_non_aktif'));
            }

            if (!Hash::check($password, $identity->password)) :
                return Redirect::back()->with('error', __('messages.kata_sandi_tidak_cocok'));
            endif;

            if ($identity && Auth::attempt(['email' => $email, 'password' => $request->password])) {
                $request->session()->regenerate();

                return redirect()->intended(route('client.auth'));
            } else {
                return Redirect::back()->with('error', __('messages.pengguna_tidak_ditemukan'));
            }
        } catch (HttpException $e) {
            return redirect(route('index'))->with('info', $e->getstatusCode() . ' | ' . $e->getMessage());
        }
    }

    function signUp()
    {
        try {
            $name = request()->name;
            $email = strtolower(request()->email);
            $password = request()->password;

            $alamat = $this->url_api() . '/auth/register';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);

            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'name' => $name,
                    'email' => $email,
                    'password' => $password
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);
            if ($response) {
                if ($response->status == true) {
                    return Redirect::back()->with('success', __('messages.pendaftaran_berhasil'));
                } else {

                    return Redirect::back()->with('info', __('messages.terjadi_kesalahan'));
                }
            } else {
                return Redirect::back()->with('info', __('messages.terjadi_kesalahan'));
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rheader = $response->getStatusCode();

            return Redirect::back()->with('error', $rheader . ' | ' . $rbody);
        }
    }

    function Forgot()
    {
        try {
            $email = strtolower(request()->email);

            $alamat = $this->url_api() . '/auth/forgot';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);

            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'email' => $email,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);
            if ($response) {
                if ($response->status == true) {
                    return Redirect::back()->with('success', __('messages.forgot_berhasil'));
                } else {

                    return Redirect::back()->with('info', __('messages.terjadi_kesalahan'));
                }
            } else {
                return Redirect::back()->with('info', __('messages.terjadi_kesalahan'));
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rheader = $response->getStatusCode();

            return Redirect::back()->with('error', $rheader . ' | ' . $rbody);
        }
    }

    function logout()
    {
        Session::flush();

        Auth::logout();

        return redirect(route('login.guest'));
    }
}
