<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Traits\MyHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Stichoza\GoogleTranslate\GoogleTranslate;

class RealtimeController extends Controller
{
    use MyHelper;

    function RealtimeFooter()
    {
        // ABOUT IBAN //
        $tr = new GoogleTranslate();

        $resume = $this->resume();

        if ($resume['status'] == true) {
            if (str_replace('_', '-', app()->getLocale()) == 'id') {
                $abouts = str_replace("\n\n", "<br/><br/>", $resume['data']->summary);
            } else {
                $abouts = $tr->setTarget('en')->translate(str_replace("\n\n", "<br/><br/>", $resume['data']->summary));
            }

            $about = '
            <div class="full-width p-0 m-0">
                <div class="page-width p-0 m-0">
                    <div class="expander p-0 m-0 foot">
                        <div class="inner-bit p-0 m-0">
                            <p class="text-left p-0 m-0">
                                ' . $abouts . '
                            </p>
                        </div>
                    </div>
                    <button class="button expand-toggle foot mt-3 mb-3" href="javascript:void(0)">
                        ' . __('messages.tampilkan_lebih_banyak') . '
                    </button>
                </div>
            </div>
            ';
        } else {
            $about = '
                Website :
                <a href="https://ibansyah.pesanin.com/" target="_blank">
                https://ibansyah.pesanin.com/
                </a>
            ';
        }

        // PAYMENT GATEWAY //
        $payment = $this->paymetnGateway();
        if ($payment['status'] == true) {
            $status_payment = true;
            $result_payment = $payment['result'];
        } else {
            $status_payment = false;
            $result_payment = NULL;
        }

        $data = [
            'abouts' => $about,
            'status_payment' => $status_payment,
            'result_payment' => $result_payment
        ];

        return response()->json($data, 200);
    }

    function RealTimeCart()
    {
        $result = '';
        $result .= '
        <div id="team" class="team">
            <div class="container">
                <div class="row">
        ';

        if (auth()->user()) {
            $alamat = $this->url_api() . '/cart/list/' . auth()->user()->id;
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);
            $request = $client->request('GET', $alamat);
            $response = $request->getBody()->getContents();
            $response = json_decode($response);
            if ($response) {
                if ($response->status == true) {
                    foreach ($response->data as $c) {
                        $size = explode(',', $c->size);
                        $variant = explode(',', $c->variant);

                        $option_sizex = '';
                        foreach ($size as $s => $key_size) {
                            if ($c->cart_size_product) {
                                if (str_replace(" ", "_", $key_size) == $c->cart_size_product) {
                                    $selected_sized = 'selected="true"';
                                    $bg_size = 'bg-primary text-white';
                                } else {
                                    $selected_sized = '';
                                    $bg_size = '';
                                }

                                $bg_sizes = $bg_size;
                                $selected_sizex = $selected_sized;
                            } else {
                                $bg_sizes = '';
                                $selected_sizex = '';
                            }
                            $option_sizex .= '<option value="' . str_replace(" ", "_", $key_size) . '" ' . $selected_sizex . ' class="' . $bg_sizes . '">' . $key_size . '</option>';
                        }

                        $option_variantx = '';
                        foreach ($variant as $v => $key_variant) {
                            if ($c->cart_variant_product) {
                                if (str_replace(" ", "_", $key_variant) == $c->cart_variant_product) {
                                    $selected_variant = 'selected="true"';
                                    $bg_variant = 'bg-primary text-white';
                                } else {
                                    $selected_variant = '';
                                    $bg_variant = '';
                                }

                                $bg_variants = $bg_variant;
                                $selected_variantx = $selected_variant;
                            } else {
                                $bg_variants = '';
                                $selected_variantx = '';
                            }

                            $option_variantx .= '<option value="' . str_replace(" ", "_", $key_variant) . '" ' . $selected_variantx . ' class="' . $bg_variants . '">' . $key_variant . '</option>';
                        }

                        if ($c->discount !== 0) {
                            $discount = $c->discount;
                            $harga = '
                            <label class="">
                                <span style="text-decoration: line-through;" class="text-muted">
                                    ' . $this->mataUang() . ' ' . number_format($c->price, 0, '.', '.') . '
                                </span>
                                &nbsp;
                                ' . $this->mataUang() . ' <label id="priceList' . $c->id_cart . '">' . number_format($c->amount_product, 0, '.', '.') . '</label>
                            </label>
                            ';
                        } else {
                            $discount = $c->discount;

                            $harga = '
                            <label class="">
                                ' . $this->mataUang() . ' <label id="priceList' . $c->id_cart . '">' . number_format($c->amount_product, 0, '.', '.') . '</label>
                            </label>
                            ';
                        }

                        if ($c->cart_size_product && $c->cart_variant_product) {
                            $checked_variant_size = 'checked';
                        } else {
                            $checked_variant_size = '';
                        }

                        $result .= '
                        <div class="col-lg-6 mb-3 p-2">

                            <input type="hidden" name="user_id" value="' . auth()->user()->id . '" class="form-control" readonly />
                            <input type="hidden" name="id_product" value="' . $c->id_product . '" class="form-control" readonly />
                            <input type="hidden" name="name_product" value="' . $c->name . '" class="form-control" readonly />
                            <input type="hidden" name="name_store" value="' . $c->name_store . '" class="form-control" readonly />
                            <input type="hidden" name="photo_store" value="' . $c->photo_store . '" class="form-control" readonly />
                            <input type="hidden" name="photo" value="' . $c->photo . '" class="form-control" readonly />
                            <input type="hidden" name="price" value="' . $c->price . '" class="form-control" readonly />
                            <input type="hidden" name="discount" value="' . $c->discount . '" class="form-control" readonly />
                            <input type="hidden" name="real_price" value="' . $c->real_price . '" id="real_price_' . $c->id_cart . '" class="form-control" readonly />
                            <input type="hidden" name="slug" value="' . $c->slug . '" class="form-control" readonly />
                            <input type="hidden" name="amount_product" id="amount_product_' . $c->id_cart . '" value="' . $c->amount_product . '" class="form-control" readonly />
                            <input type="hidden" name="total_product" id="total_product_' . $c->id_cart . '" value="' . $c->total_product . '" class="form-control" readonly />

                            <div class="member d-flex align-items-start p-2 col-cart_' . $c->id_cart . '">
                                <div class="pic">
                                    <a href="' . route('product.detail', $c->slug) . '">
                                        <img src="' . $c->photo . '" class="img-fluid" alt="' . $c->name . '" title="' . $c->name . '" />
                                    </a>
                                </div>
                                <div class="member-info w-100">
                                    <div class="d-flex w-100 d-inline-block p-0 m-0">

                                        <h4 class="mr-4">
                                            ' . $c->name . '
                                        </h4>
                                        <button class="btn btn-outline-danger float-right ml-5 btn-delete" align="right" onclick="deleteCart_' . $c->id_cart . '()" type="button">
                                            <i class="bx bx-trash"></i>
                                        </button>
                                    </div>
                                    <label>
                                        <div class="d-flex">
                                            <img src="' . $c->photo_store . '" class="rounded-full mr-2 w-10" alt="' . $c->name_store . '" title="' . $c->name_store . '" />
                                            <span class="mt-2">' . $c->name_store . '</span>
                                        </div>
                                    </label>
                                    <div class="social p-2">
                                        <a href="#" class="mr-4">' . $discount . '<i class="ri-percent-line"></i></a>
                                        ' . $harga . '
                                    </div>
                                    <p class="fw-bold">
                                        ' . __('messages.pilih_size') . ' <label class="text-danger">*</label> :
                                        <select class="form-control mt-3 mb-3" id="selectSize' . $c->id_cart . '" required>
                                            <option value="" selected>' . __('messages.pilih_size') . '</option>
                                            ' . $option_sizex . '
                                        </select>

                                        <input type="hidden" name="size" id="valxSize_' . $c->id_cart . '" value="' . $c->cart_size_product . '" class="form-control" readonly />

                                        <script>
                                            $("#selectSize' . $c->id_cart . '").change(function(){
                                                $("#valxSize_' . $c->id_cart . '").val($("#selectSize' . $c->id_cart . '").val());
                                                $.ajax({
                                                    header: {
                                                        "X-CSRF-TOKEN": $("input[name=_token]").val()
                                                    }
                                                    , url: "' . url("") . '/update/cart/size/' . $c->id_cart . '/" + $("#selectSize' . $c->id_cart . '").val()
                                                    , type: "GET"
                                                    , cache: false
                                                    , dataType: "JSON"
                                                    , success: function(resp_load_update_size) {
                                                        $("#valxSize_' . $c->cart_size_product . '").val();
                                                    }
                                                });

                                                if($("#selectSize' . $c->id_cart . '").val() !== "")
                                                {
                                                    $(".bayar-btn").prop("disabled", false);
                                                } else {
                                                    $(".bayar-btn").prop("disabled", true);
                                                }
                                            });

                                            $("#selectSize' . $c->id_cart . '").val($("#valxSize_' . $c->id_cart . '").val());
                                        </script>
                                    </p>
                                    <p class="fw-bold">
                                        ' . __('messages.pilih_variant') . ' <label class="text-danger">*</label> :
                                        <select class="form-control mt-3 mb-3" id="selectVariant' . $c->id_cart . '" required>
                                            <option value="" selected>' . __('messages.pilih_variant') . '</option>
                                            ' . $option_variantx . '
                                        </select>

                                        <input type="hidden" name="variant" id="valxVariant_' . $c->id_cart . '" value="' . $c->cart_variant_product . '" class="form-control" readonly />

                                        <script>
                                            $("#selectVariant' . $c->id_cart . '").change(function(){
                                                $("#valxVariant_' . $c->id_cart . '").val($("#selectVariant' . $c->id_cart . '").val());
                                                $.ajax({
                                                    header: {
                                                        "X-CSRF-TOKEN": $("input[name=_token]").val()
                                                    }
                                                    , url: "' . url("") . '/update/cart/variant/' . $c->id_cart . '/" + $("#selectVariant' . $c->id_cart . '").val()
                                                    , type: "GET"
                                                    , cache: false
                                                    , dataType: "JSON"
                                                    , success: function(resp_load_update_variant) {
                                                        $("#valxVariant_' . $c->cart_variant_product . '").val();
                                                    }
                                                });

                                                if($("#selectVariant' . $c->id_cart . '").val() !== "")
                                                {
                                                    $(".bayar-btn").prop("disabled", false);
                                                } else {
                                                    $(".bayar-btn").prop("disabled", true);
                                                }
                                            });
                                            $("#selectVariant' . $c->id_cart . '").val($("#valxVariant_' . $c->id_cart . '").val());
                                        </script>
                                    </p>

                                    <p>
                                        <div class="qty-input input-qty_' . $c->id_cart . '">
                                            <button class="qty-count qty-count--minus qty-minus_' . $c->id_cart . ' count-qty_' . $c->id_cart . '" data-action="minus" type="button">-</button>
                                            <input class="product-qty qty-product_' . $c->id_cart . '" type="number" name="total_product" min="1" max="' . $c->stocks_product . '" value="' . $c->total_product . '">
                                            <button class="qty-count qty-count--add qty-add_' . $c->id_cart . ' count-qty_' . $c->id_cart . '" data-action="add" type="button">+</button>
                                        </div>
                                    </p>

                                    ' . csrf_field() . '

                                    <script>
                                        $(".grand-total").html();
                                        function deleteCart_' . $c->id_cart . '(){
                                            $.ajax({
                                                header: {
                                                    "X-CSRF-TOKEN":  $("input[name=_token]").val()
                                                }
                                                , url: "' . route("delete.cart", $c->id_cart) . '"
                                                , type: "GET"
                                                , dataType: "JSON"
                                                , success: function(resp_load_delete) {
                                                    if(resp_load_delete.status == true){
                                                        $.ajax({
                                                            header: {
                                                                "X-CSRF-TOKEN": $("input[name=_token]").val()
                                                            }
                                                            , url: "' . route("realtime.cart") . '"
                                                            , type: "POST"
                                                            , cache: false
                                                            , data: {
                                                                "_token":  $("input[name=_token]").val()
                                                            }
                                                            , dataType: "JSON"
                                                            , beforeSend: function() {
                                                                $("#cart-list").html("<div class=\'alert alert-info\'>' . __("messages.please_wait") . '</div>");
                                                            }
                                                            , success: function(resp_load_cart) {
                                                                $("#cart-list").html(resp_load_cart.result);
                                                                $(".count_cart").html("(" + resp_load_cart.count_cart + ")")
                                                            }
                                                            , error: function() {
                                                                $("#cart-list").html("<div class=\'alert alert-danger\'>' . __("messages.terjadi_kesalahan") . '</div>");
                                                            }
                                                        });
                                                    } else {
                                                        Swal.fire({
                                                            icon: "error",
                                                            title: "Oopss ...",
                                                            html: "' . __('messages.terjadi_kesalahan') . '",
                                                            showConfirmButton: false,
                                                            timer: 1500
                                                        });
                                                    }
                                                }
                                            });
                                        }

                                        var QtyInput_' . $c->id_cart . ' = (function () {
                                            var $qtyInputs_' . $c->id_cart . ' = $(".input-qty_' . $c->id_cart . '");

                                            if (!$qtyInputs_' . $c->id_cart . '.length) {
                                                return;
                                            }

                                            var $inputs_' . $c->id_cart . ' = $qtyInputs_' . $c->id_cart . '.find(".qty-product_' . $c->id_cart . '");
                                            var $countBtn_' . $c->id_cart . ' = $qtyInputs_' . $c->id_cart . '.find(".count-qty_' . $c->id_cart . '");
                                            var qtyMin_' . $c->id_cart . ' = parseInt($inputs_' . $c->id_cart . '.attr("min"));
                                            var qtyMax_' . $c->id_cart . ' = parseInt($inputs_' . $c->id_cart . '.attr("max"));

                                            $inputs_' . $c->id_cart . '.change(function () {
                                                var $this_' . $c->id_cart . ' = $(this);
                                                var $minusBtn_' . $c->id_cart . ' = $this_' . $c->id_cart . '.siblings(".qty-minus_' . $c->id_cart . '");
                                                var $addBtn_' . $c->id_cart . ' = $this_' . $c->id_cart . '.siblings(".qty-add_' . $c->id_cart . '");
                                                var qty_' . $c->id_cart . ' = parseInt($this_' . $c->id_cart . '.val());

                                                if (isNaN(qty_' . $c->id_cart . ') || qty_' . $c->id_cart . ' <= qtyMin_' . $c->id_cart . ') {
                                                    $this_' . $c->id_cart . '.val(qtyMin_' . $c->id_cart . ');
                                                    $minusBtn_' . $c->id_cart . '.prop("disabled", true);
                                                } else {
                                                    $minusBtn_' . $c->id_cart . '.prop("disabled", false);

                                                    if(qty_' . $c->id_cart . ' >= qtyMax_' . $c->id_cart . '){
                                                        $this_' . $c->id_cart . '.val(qtyMax_' . $c->id_cart . ');
                                                        $addBtn_' . $c->id_cart . '.prop("disabled", true);
                                                    } else {
                                                        $this_' . $c->id_cart . '.val(qty_' . $c->id_cart . ');
                                                        $addBtn_' . $c->id_cart . '.prop("disabled", false);
                                                    }
                                                }
                                            });

                                            $countBtn_' . $c->id_cart . '.click(function () {
                                                var operator_' . $c->id_cart . ' = this.dataset.action;
                                                var $this_' . $c->id_cart . ' = $(this);
                                                var $input_' . $c->id_cart . ' = $this_' . $c->id_cart . '.siblings(".qty-product_' . $c->id_cart . '");
                                                var qty_' . $c->id_cart . ' = parseInt($input_' . $c->id_cart . '.val());

                                                if (operator_' . $c->id_cart . ' == "add") {
                                                    qty_' . $c->id_cart . ' += 1;
                                                    if (qty_' . $c->id_cart . ' >= qtyMin_' . $c->id_cart . ' + 1) {
                                                        $this_' . $c->id_cart . '.siblings(".qty-minus_' . $c->id_cart . '").prop("disabled", false);
                                                    }

                                                    if (qty_' . $c->id_cart . ' >= qtyMax_' . $c->id_cart . ') {
                                                        $this_' . $c->id_cart . '.prop("disabled", true);
                                                    }

                                                    $.ajax({
                                                        header: {
                                                            "X-CSRF-TOKEN": $("input[name=_token]").val()
                                                        }
                                                        , url: "' . url("") . '/update/cart/add/' . $c->id_cart . '/" + qty_' . $c->id_cart . '
                                                        , type: "GET"
                                                        , cache: false
                                                        , dataType: "JSON"
                                                        , success: function(resp_load_update_add) {
                                                            console.log(resp_load_update_add);
                                                        }
                                                    });


                                                } else {
                                                    qty_' . $c->id_cart . ' = qty_' . $c->id_cart . ' <= qtyMin_' . $c->id_cart . ' ? qtyMin_' . $c->id_cart . ' : (qty_' . $c->id_cart . ' -= 1);

                                                    if (qty_' . $c->id_cart . ' == qtyMin_' . $c->id_cart . ') {
                                                        $this_' . $c->id_cart . '.prop("disabled", true);
                                                    }

                                                    if (qty_' . $c->id_cart . ' < qtyMax_' . $c->id_cart . ') {
                                                        $this_' . $c->id_cart . '.siblings(".qty-add_' . $c->id_cart . '").prop("disabled", false);
                                                    }

                                                    $.ajax({
                                                        header: {
                                                            "X-CSRF-TOKEN": $("input[name=_token]").val()
                                                        }
                                                        , url: "' . url("") . '/update/cart/minus/' . $c->id_cart . '/" + qty_' . $c->id_cart . '
                                                        , type: "GET"
                                                        , cache: false
                                                        , dataType: "JSON"
                                                        , success: function(resp_load_update_minus) {
                                                            console.log(resp_load_update_minus);
                                                        }
                                                    });
                                                }

                                                $input_' . $c->id_cart . '.val(qty_' . $c->id_cart . ');
                                                var harga_' . $c->id_cart . ' = parseInt("' . $c->amount_product . '");
                                                var real_' . $c->id_cart . ' = parseInt(qty_' . $c->id_cart . ');
                                                var total_harga_' . $c->id_cart . ' = real_' . $c->id_cart . ' * harga_' . $c->id_cart . ';

                                                $("#total_product_' . $c->id_cart . '").val($input_' . $c->id_cart . '.val());
                                                $("#amount_product_' . $c->id_cart . '").val(total_harga_' . $c->id_cart . ');
                                                $("#priceList' . $c->id_cart . '").html(format_' . $c->id_cart . '(total_harga_' . $c->id_cart . '));

                                                function format_' . $c->id_cart . '(num){
                                                    var str_' . $c->id_cart . ' = num.toString().replace("", ""),
                                                    parts_' . $c->id_cart . ' = false,
                                                    output_' . $c->id_cart . ' = [], i = 1, formatted = null;
                                                    if(str_' . $c->id_cart . '.indexOf(".") > 0) {
                                                      parts_' . $c->id_cart . ' = str_' . $c->id_cart . '.split(".");
                                                      str_' . $c->id_cart . ' = parts_' . $c->id_cart . '[0];
                                                    }
                                                    str_' . $c->id_cart . ' = str_' . $c->id_cart . '.split("").reverse();
                                                    for(var j_' . $c->id_cart . ' = 0, len_' . $c->id_cart . ' = str_' . $c->id_cart . '.length; j_' . $c->id_cart . ' < len_' . $c->id_cart . '; j_' . $c->id_cart . '++) {
                                                      if(str_' . $c->id_cart . '[j_' . $c->id_cart . '] != ".") {
                                                        output_' . $c->id_cart . '.push(str_' . $c->id_cart . '[j_' . $c->id_cart . ']);
                                                        if(i%3 == 0 && j_' . $c->id_cart . ' < (len_' . $c->id_cart . ' - 1)) {
                                                          output_' . $c->id_cart . '.push(".");
                                                        }
                                                        i++;
                                                      }
                                                    }
                                                    formatted_' . $c->id_cart . ' = output_' . $c->id_cart . '.reverse().join("");
                                                    return("" + formatted_' . $c->id_cart . ' + ((parts_' . $c->id_cart . ') ? "." + parts_' . $c->id_cart . '[1].substr(0, 2) : ""));
                                                };
                                            });
                                        })();
                                    </script>
                                </div>
                            </div>
                        </div>
                        ';
                    }

                    $count_cart = number_format(count($response->data), 0, '.', '.');
                } else {
                    $count_cart = number_format(0, 0, '.', '.');
                    $result .= '
                    <div class="col-lg-12" data-aos="fade-up" data-aos-delay="100">
                        <div class="member d-flex align-items-start">
                            <div class="pic">
                                <img src="' . config("app.url") . '/assets/img/avatar404.png" class="img-fluid" alt="' . __('messages.keranjang_kosong') . '" title="' . __('messages.keranjang_kosong') . '" />
                            </div>
                            <div class="member-info mt-5" align="center">
                                <h4>' . __('messages.keranjang_kosong') . '</h4>
                            </div>
                        </div>
                    </div>
                    ';
                }
            } else {
                $count_cart = number_format(0, 0, '.', '.');
                $result .= '
                <div class="col-lg-12" data-aos="fade-up" data-aos-delay="100">
                    <div class="member d-flex align-items-start">
                        <div class="pic">
                            <img src="' . config("app.url") . '/assets/img/avatar404.png" class="img-fluid" alt="' . __('messages.terjadi_kesalahan') . '" title="' . __('messages.terjadi_kesalahan') . '" />
                        </div>
                        <div class="member-info mt-5" align="center">
                            <h4>' . __('messages.keranjang_kosong') . '</h4>
                        </div>
                    </div>
                </div>
                ';
            }
        } else {
            $count_cart = number_format(0, 0, '.', '.');
            $result .= '
            <div class="col-lg-12" data-aos="fade-up" data-aos-delay="100">
                <div class="member d-flex align-items-start">
                    <div class="pic">
                        <img src="https://img.freepik.com/free-vector/flat-design-cafe-enter-sign_23-2149276119.jpg?w=2000" class="img-fluid" alt="' . __('messages.login_dahulu') . '" title="' . __('messages.login_dahulu') . '" />
                    </div>
                    <div class="member-info mt-5" align="center">
                        <h4 class="text-center">' . __('messages.login_dahulu') . ' ....</h4>
                    </div>
                </div>
            </div>
            ';
        }

        $result .= '
                    </div>
                </div>
            </div>
        ';

        $data = [
            'count_cart' => $count_cart,
            'result' => $result
        ];

        return response()->json($data, 200);
    }

    function AddCart($productID)
    {
        try {
            $userID = auth()->user() ? auth()->user()->id : NULL;
            $alamat = $this->url_api() . '/cart/store';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id_product' => $productID,
                    'user_id' => $userID,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function RealGrandTotal()
    {
        try {
            $userID = auth()->user() ? auth()->user()->id : NULL;
            $alamat = $this->url_api() . '/cart/sum/' . auth()->user()->id;
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('GET', $alamat);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                    $datas1 = $this->mataUang() . ' ' . number_format($response->data, 0, '.', '.');
                    $datas2 = $response->data;
                } else {
                    $status = false;
                    $datas1 = $this->mataUang() . ' ' . number_format(0, 0, '.', '.');
                    $datas2 = 0;
                }

                $statuses = $status;
                $data1 = $datas1;
                $data2 = $datas2;
            } else {
                $statuses = false;
                $data1 = $this->mataUang() . ' ' . number_format(0, 0, '.', '.');
                $data2 = 0;
            }
            $data = [
                'status' => $statuses,
                'data1' => $data1,
                'data2' => $data2
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }
    function UpdateCartAdd($id, $add)
    {
        try {
            $alamat = $this->url_api() . '/cart/plus-cart';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id,
                    'value' => $add,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function UpdateCartMinus($id, $minus)
    {
        try {
            $alamat = $this->url_api() . '/cart/minus-cart';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id,
                    'value' => $minus,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function UpdateCartSize($id, $value_size)
    {
        try {
            $alamat = $this->url_api() . '/cart/update';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id,
                    'size' => $value_size,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function UpdateCartVariant($id, $value_variant)
    {
        try {
            $alamat = $this->url_api() . '/cart/update';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id,
                    'variant' => $value_variant,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function DeleteCart($id)
    {
        try {
            $alamat = $this->url_api() . '/cart/delete';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function AddWishlist($id_product)
    {
        try {
            $alamat = $this->url_api() . '/wishlist/store';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id_product' => $id_product,
                    'user_id' => auth()->user()->id
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function deleteWishlist($id)
    {
        try {
            $alamat = $this->url_api() . '/wishlist/delete';
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            $request = $client->request('POST', $alamat, [
                'form_params' => [
                    'id' => $id,
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response);

            if ($response) {
                if ($response->status == true) {
                    $status = true;
                } else {
                    $status = false;
                }

                $statuses = $status;
            } else {
                $statuses = false;
            }

            $data = [
                'status' => $statuses
            ];

            return response()->json($data, 200);
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $rbody = $response->getReasonPhrase();
            $rcode = $response->getStatusCode();
            $rheader = $response->getHeaders();

            $data = [
                'body' => $rbody,
                'code' => $rcode,
                // 'header' => $rheader
            ];

            return response()->json($data, 200);
        }
    }

    function RealTimeBanner()
    {
        $alamat = $this->url_api() . '/product/list';
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
        $request = $client->request('GET', $alamat);
        $response = $request->getBody()->getContents();
        $response = json_decode($response);

        if ($response) {
            if ($response->status == true) {
                $result = '';
                foreach ($response->data as $p) {
                    if ($p->id_product == 1) {
                        $set_active = 'active';
                    } else {
                        $set_active = '';
                    }

                    if (auth()->user()) {
                        if (auth()->user()->id == $p->id_user) {
                            $classes = 'd-none';
                        } else {
                            $classes = '';
                        }

                        $hiden = $classes;
                    } else {
                        $hiden = '';
                    }

                    if ($p->discount == 0) {
                        $discount = '';
                        $harga = '
                        <p class="animate__animated animate__fadeInUp">
                            ' . $this->mataUang() . ' ' . number_format($p->real_price, 0, '.', '.') . '
                        </p>
                        ';
                    } else {
                        $discount = '
                        <p class="animate__animated animate__fadeInUp badge bg-danger">
                            ' . $p->discount . '%
                        </p>
                        ';
                        $harga = '
                        <p class="animate__animated animate__fadeInUp">
                            <span style="text-decoration: line-through;" class="text-muted">
                                ' . $this->mataUang() . ' ' . number_format($p->price, 0, '.', '.') . '
                            </span>
                            &nbsp;
                            ' . $this->mataUang() . ' ' . number_format($p->real_price, 0, '.', '.') . '
                        </p>
                        ';
                    }

                    $result .= '
                    <div class="carousel-item ' . $set_active . '">
                        <div class="carousel-container">
                            <h2 class="animate__animated animate__fadeInDown text-center">
                                <center>
                                    <img src="' . $p->photo . '" class="img-fluid" alt="' . $p->name . '" title="' . $p->name . '" />
                                </center>
                            </h2>
                            <p class="animate__animated animate__fadeInUp">
                                ' . $p->name . '
                            </p>
                            ' . $discount . '
                            ' . $harga . '
                            <a href="' . route('product.detail', $p->slug) . '" class="' . $hiden . ' btn-get-started animate__animated animate__fadeInUp">
                                ' . __('messages.lihat_detail') . '
                            </a>
                        </div>
                    </div>
                    ';
                }
            } else {
                $result = '
                <div class="carousel-item active">
                    <div class="carousel-container">
                        <h2 class="animate__animated animate__fadeInDown">' . __('messages.welcome') . ' <span>' . config('app.brand') . '</h2>
                        <p class="animate__animated animate__fadeInUp"></p>
                        <a href="' . route('product') . '" class="btn-get-started animate__animated animate__fadeInUp">
                            ' . __('messages.mulai_belanja') . '
                        </a>
                    </div>
                </div>
                ';
            }
        } else {
            $result = '
                <div class="carousel-item active">
                    <div class="carousel-container">
                        <h2 class="animate__animated animate__fadeInDown">' . __('messages.welcome') . ' <span>' . config('app.brand') . '</h2>
                        <p class="animate__animated animate__fadeInUp"></p>
                        <a href="' . route('product') . '" class="btn-get-started animate__animated animate__fadeInUp">
                            ' . __('messages.mulai_belanja') . '
                        </a>
                    </div>
                </div>
                ';
        }

        $data = [
            'result' => $result
        ];

        return response()->json($data, 200);
    }

    function RealTimeSubscribe()
    {
        $email = request()->email;
        $ip = $this->userAgentIp();

        $alamat = $this->url_api() . '/subscribe/store';
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
        $request = $client->request('POST', $alamat, [
            'form_params' => [
                'email' => $email,
                'ip' => $ip
            ]
        ]);
        $response = $request->getBody()->getContents();
        $response = json_decode($response);

        if ($response) {
            if ($response->status == true) {
                return Redirect::back()->with('success', __('messages.subscribe_success'));
            } else {
                return Redirect::back()->with('info', __('messages.data_tersedia'));
            }
        } else {
            return Redirect::back()->with('info', __('messages.data_tersedia'));
        }
    }
}
